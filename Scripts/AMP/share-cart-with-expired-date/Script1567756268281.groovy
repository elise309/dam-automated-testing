import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.time.LocalDate as LocalDate

'generate expired date : yesterday, today, tomorrow'
def dateList = [getDate(-1), getDate(0), getDate(1)]

'To run 3 times test cases: share cart with expired/valid date/empty date'
for (def date : dateList) {
    WebUI.callTestCase(findTestCase('Login/login_home page'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.click(findTestObject('AMP/button_add to cart'))

    WebUI.click(findTestObject('AMP/button_cart modal'))

    WebUI.verifyElementPresent(findTestObject('AMP/dialog_cart'), 10)

    WebUI.click(findTestObject('AMP/button_share cart'))

    WebUI.verifyElementPresent(findTestObject('AMP/dialog_share'), 10)

    WebUI.setText(findTestObject('AMP/txt_email address'), GlobalVariable.email)

    WebUI.setText(findTestObject('AMP/txt_ message'), GlobalVariable.message)

    WebUI.focus(findTestObject('AMP/calendar_expired date'))

    WebUI.executeJavaScript('$(\':input\').removeAttr(\'readonly\')', null)

    WebUI.setText(findTestObject('AMP/txt_expired date'), date)

    WebUI.click(findTestObject('AMP/button_dialog share'))

    WebUI.verifyElementPresent(findTestObject('AMP/dialog_show success'), 10)

    WebUI.click(findTestObject('AMP/button_close'))

    WebUI.closeBrowser()
}

String getDate(Integer offset) {
    LocalDate today = LocalDate.now()

    LocalDate offsetDay = today.plusDays(offset)

    return offsetDay.toString()
}

