import org.openqa.selenium.By as By
import org.openqa.selenium.support.ui.Select as Select
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login/login_home page'), [('username') : 'asicsadmin'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('AMP/button_add all to cart'))

'Verify add all dialog shows out'
WebUI.verifyElementPresent(findTestObject('AMP/dialog_add all'), 10)

WebUI.waitForElementClickable(findTestObject('AMP/button_confirm'), 10)

WebUI.delay(10)

WebUI.click(findTestObject('AMP/button_confirm'))

'Verify all assets added'
WebUI.verifyElementPresent(findTestObject('AMP/icon_cart count 150'), 10)

WebUI.closeBrowser()

