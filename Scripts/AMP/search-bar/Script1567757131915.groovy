import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login/login_home page'), [('username') : 'asicsadmin'], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('AMP/button_Search'), 10)

WebUI.setText(findTestObject('AMP/input_fulltext'), '123321')

WebUI.click(findTestObject('AMP/button_Search'))

WebUI.verifyElementVisible(findTestObject('AMP/txt_not matched assets'))

WebUI.clearText(findTestObject('AMP/input_fulltext'))

WebUI.click(findTestObject('AMP/button_Search'))

WebUI.delay(8)

String firstArticleText = WebUI.getText(findTestObject('AMP/text_first article'))

WebUI.setText(findTestObject('AMP/input_fulltext'), firstArticleText)

WebUI.click(findTestObject('AMP/button_Search'))

WebUI.verifyElementVisible(findTestObject('AMP/text_first article'))

WebUI.closeBrowser()

