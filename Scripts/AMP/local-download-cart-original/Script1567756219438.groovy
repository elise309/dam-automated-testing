import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login/login_home page'), [:], FailureHandling.STOP_ON_FAILURE)

'Add an asset to cart'
WebUI.click(findTestObject('AMP/button_add to cart'))

'Verify asset added to cart'
WebUI.verifyElementPresent(findTestObject('AMP/icon_cart count 1'), 10)

'Open the cart'
WebUI.click(findTestObject('AMP/button_cart modal'))

WebUI.verifyElementPresent(findTestObject('AMP/dialog_cart'), 10)

'Click "download" button in cart dialog'
WebUI.click(findTestObject('AMP/button_cart download'))

WebUI.verifyElementPresent(findTestObject('AMP/dialog_download cart'), 10)

'Select "original" rendition for the download asset'
WebUI.click(findTestObject('AMP/rendition_Original'))

'Click "download" button to start download'
WebUI.click(findTestObject('AMP/button_cart download'))

'Verify download successfully'
WebUI.verifyElementPresent(findTestObject('AMP/F12_download token'), 10)

WebUI.closeBrowser()

