import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login/login_home page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('AMP/First article'), 10)

'Click the first asset in search result'
WebUI.click(findTestObject('AMP/First article'))

'Verify the asset detail page open'
WebUI.verifyElementPresent(findTestObject('AMP/header_renditions'), 10)

'Click the left icon to leave detail page'
WebUI.click(findTestObject('AMP/icon_chevron left link'), FailureHandling.STOP_ON_FAILURE)

'Verify the detail page not show'
WebUI.verifyElementNotPresent(findTestObject('AMP/header_renditions'), 10)

WebUI.closeBrowser()

