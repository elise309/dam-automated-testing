import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login/login_home page'), [:], FailureHandling.STOP_ON_FAILURE)

'Click the first asset\'s download button in search result'
WebUI.click(findTestObject('AMP/button_download'))

'Verify the download dialog shows out'
WebUI.verifyElementPresent(findTestObject('AMP/dialog_download'), 10)

'Select "original" rendition in download dialog'
WebUI.click(findTestObject('AMP/rendition_Original'))

WebUI.selectOptionByValue(findTestObject('AMP/select_rendition'), 
    'original', true)

'Click "download" button in download dialog'
WebUI.click(findTestObject('AMP/button_dialog download'))

'Verify download the asset successfully'
WebUI.verifyElementPresent(findTestObject('AMP/F12_download token'), 10)

WebUI.closeBrowser()

