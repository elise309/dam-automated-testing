import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login/login_home page'), [:], FailureHandling.STOP_ON_FAILURE)

'Add all assets to cart'
WebUI.click(findTestObject('AMP/button_add all to cart'))

WebUI.delay(10, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('AMP/button_confirm'))

'Verify all assets added'
WebUI.verifyElementPresent(findTestObject('AMP/icon_cart count 150'), 10)

WebUI.delay(10, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('AMP/button_cart modal'))

WebUI.verifyElementPresent(findTestObject('AMP/dialog_cart'), 10)

WebUI.click(findTestObject('AMP/button_cart download'))

WebUI.verifyElementPresent(findTestObject('AMP/dialog_cart download'), 10)

WebUI.delay(10, FailureHandling.STOP_ON_FAILURE)

'Select the download assets\' rendition'
WebUI.click(findTestObject('AMP/rendition_Original'))

WebUI.click(findTestObject('AMP/button_cart download'))

'Message shows "almost complete, please see your email..."'
WebUI.verifyElementPresent(findTestObject('AMP/message_email sent'), 10)

WebUI.closeBrowser()

