import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://brandportal.asics.com/content/amp/en/login.html?resource=%2Fcontent%2Famp%2Fen%2Fhome.html&$$login$$=%24%24login%24%24&j_reason=unknown&j_reason_code=unknown')

WebUI.click(findTestObject('Object Repository/SignInBrandPortal/Page_Login/div_Login'))

WebUI.verifyElementPresent(findTestObject('SignInBrandPortal/message_userID must have a value'), 10)

WebUI.verifyElementPresent(findTestObject('SignInBrandPortal/message_password at least 5'), 10)

WebUI.verifyElementPresent(findTestObject('SignInBrandPortal/message_password must have a value'), 10)

