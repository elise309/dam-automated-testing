import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://author1.63-stage.asics-assets.adobecqms.net/content/amp/en/home.html')

WebUI.maximizeWindow()

WebUI.waitForElementPresent(findTestObject('AMP/txt_username'), 10)

WebUI.setText(findTestObject('AMP/txt_username'), username)

WebUI.setEncryptedText(findTestObject('AMP/txt_password'), 'DkwnXQ9oiEcTNYGGn0rs4MrdKEstzuzm0u44E/6U/tQ=')

WebUI.click(findTestObject('AMP/button_Sign In'))

WebUI.verifyElementPresent(findTestObject('AMP/a_HOME'), 10)

