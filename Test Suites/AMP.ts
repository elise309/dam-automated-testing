<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>AMP</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0583a9af-e8fc-4289-afcc-9500c1857f02</testSuiteGuid>
   <testCaseLink>
      <guid>00ea9481-6fa6-42b5-a7e8-82f6ec6f4822</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/add-all-to-cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cba143f3-45f1-47e3-8d32-118cbc2bc964</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/add-to-cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9885ac11-af79-41bc-8179-f7d50a00223e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/asset-detail-page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2775db4-f587-4ebd-96ad-89e6959a62f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/bulk-download-cart-original</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0faf2111-892a-439d-910c-65ffac0a0d21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/bulk-download-cart-rendition</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c71d6894-d026-4bf8-976b-7ea4a496a7bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/cancel-add-all</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35c77137-ce7b-44e6-a8d0-87ed5579983f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/cancel-download</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aaa0cd13-eca5-4059-bd0f-a20242096bd2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/cancel-share</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6dbafe7-d549-4afe-9d50-dbec27eb1e80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/clear-cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0dd00ed1-0f4f-4c26-90bc-31da4729065d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/date-range</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>322788aa-d022-4561-a2d1-fd87061fd9c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/download-asset</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>792bff25-7299-4480-8067-5b895dc1436f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/layout-toggle</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5150afd0-7497-44aa-9280-7c0fe6f589ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/local-download-cart-original</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>661ba713-1a42-48bf-8b52-69709c7bdb1c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/local-download-cart-rendition</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f01c383-0810-413f-90c6-ea9f8b671661</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/portal-links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d059ee1f-e5dd-44ca-9634-5752e6808e51</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/property-filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77908134-f8d4-4551-963e-88f99d16d067</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/search-bar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8a7197d-9373-46bd-812c-b371f399271e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/share-asset-with-expired-date</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>86153510-52d1-4c43-b625-e57669ccee74</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>47fdeaac-8656-482b-bc82-4eb0835f0737</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/share-cart-with-expired-date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41d27c3f-1e5b-4c9c-91af-e4ba7c00fd33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMP/tags-filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd52523b-9c9a-445d-894b-dcee438f3709</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LoginBrandPortal/classic login with asics user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a553e2b-043a-4107-abbf-e4af8c2201c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LoginBrandPortal/classic login with classic user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10369ff7-2704-4dcd-92c4-c11372d406e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LoginBrandPortal/classic login with error password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2497d51-bdc1-4e5d-8375-5776b1378ad8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LoginBrandPortal/classic reset password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>862de893-9708-4b2a-991e-5cb46c213e34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LoginBrandPortal/Form validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdbb327d-3c9d-4b2c-b0b6-665f13c493a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LoginBrandPortal/SSO cannot reset password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fa6b1cc-6055-4d89-a8a4-65ec60e15338</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/login_home page</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3e29dbf2-0712-417e-ad39-11eb5e5d1dee</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
