<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>use SSO to login</name>
   <tag></tag>
   <elementGuidId>30f8e9eb-db67-4c76-80a2-013fd6922253</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[(text() = 'Please use sso to login' or . = 'Please use sso to login')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Please use sso to login</value>
   </webElementProperties>
</WebElementEntity>
