<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>please use SSO</name>
   <tag></tag>
   <elementGuidId>0f7c9477-2014-4cc6-94e8-b7932787cb72</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[(text() = concat('If you' , &quot;'&quot; , 're an ASICS user, please use SSO') or . = concat('If you' , &quot;'&quot; , 're an ASICS user, please use SSO'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>path</name>
      <type>Main</type>
      <value>id(&quot;login-form&quot;)/div[@class=&quot;ui error message&quot;]/ul[@class=&quot;list&quot;]/li[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>If you're an ASICS user, please use SSO</value>
   </webElementProperties>
</WebElementEntity>
