<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Reset password email sent</name>
   <tag></tag>
   <elementGuidId>1ac8ce72-cbd3-42bb-9c79-ac5fc41584e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Password reset request success.
            Please, check your inbox.
        ' or . = 'Password reset request success.
            Please, check your inbox.
        ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Password reset request success.
            Please, check your inbox.
        </value>
   </webElementProperties>
</WebElementEntity>
