<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_asset share</name>
   <tag></tag>
   <elementGuidId>8f7731f2-bb1e-4ba7-a448-5e0210676e79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul li:nth-child(2)</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//button[@class = 'ui link button' and @data-asset-share-id = 'share-asset' and @data-asset-share-asset = '/content/dam/product/9/2019_2/footwear/global/1011A636_750_SR_FL.png' and (text() = '
                        Share
                    ' or . = '
                        Share
                    ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//article[@id='/content/dam/product/9/2019_2/footwear/global/Copy of 1011A636_750_SR_FR.png/Copy of 1011A636_750_SR_FR.png']/div[2]/ul/li[2]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//article[@id='/content/dam/product/9/2019_2/footwear/global/Copy of 1011A636_750_SR_FR.png/Copy of 1011A636_750_SR_FR.png']/div[2]/ul/li[2]/button</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to Cart'])[1]/preceding::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove From Cart'])[1]/preceding::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/button</value>
   </webElementXpaths>
</WebElementEntity>
