<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dialog_cart</name>
   <tag></tag>
   <elementGuidId>1439b7b9-c2a2-4d61-a13a-8290a8acf5a4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'ui modal cmp-modal-cart--wrapper cmp-modal transition visible active']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui modal cmp-modal-cart--wrapper cmp-modal transition visible active</value>
   </webElementProperties>
</WebElementEntity>
