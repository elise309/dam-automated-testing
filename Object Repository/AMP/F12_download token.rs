<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>F12_download token</name>
   <tag></tag>
   <elementGuidId>422aaac6-0cbc-4dd2-93ae-f03bf6de0455</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = ':cq_csrf_token']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>:cq_csrf_token</value>
   </webElementProperties>
</WebElementEntity>
