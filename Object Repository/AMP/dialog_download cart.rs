<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dialog_download cart</name>
   <tag></tag>
   <elementGuidId>836f0cb4-7461-4c3c-968b-2a74c13c7d80</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'ui modal cmp-modal-download--wrapper cmp-modal transition visible active']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui modal cmp-modal-download--wrapper cmp-modal transition visible active</value>
   </webElementProperties>
</WebElementEntity>
