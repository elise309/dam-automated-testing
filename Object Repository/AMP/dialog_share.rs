<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dialog_share</name>
   <tag></tag>
   <elementGuidId>e56b1d99-e808-4cbe-984e-96ab27cd3798</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'ui form modal cmp-modal-share__wrapper--initial  cmp-modal transition visible active']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui form modal cmp-modal-share__wrapper--initial  cmp-modal transition visible active</value>
   </webElementProperties>
</WebElementEntity>
