<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dialog_add all</name>
   <tag></tag>
   <elementGuidId>0a6d94a9-dfc3-4d97-9f51-2fe93adece5a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'addAllToCartConfirmationContent']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>addAllToCartConfirmationContent</value>
   </webElementProperties>
</WebElementEntity>
