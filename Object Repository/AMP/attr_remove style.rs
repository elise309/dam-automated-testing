<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>attr_remove style</name>
   <tag></tag>
   <elementGuidId>5e7137b5-ccc5-433d-b614-cb9eee05e745</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>ul li:nth-child(3) button:nth-child(2)</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>ul li:nth-child(3) button:nth-child(2)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>style</name>
      <type>Main</type>
      <value>display: none;</value>
   </webElementProperties>
</WebElementEntity>
