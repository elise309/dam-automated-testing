<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_close</name>
   <tag></tag>
   <elementGuidId>fab7d62b-f956-4441-966f-3518900b6c0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>.cmp-modal-share__wrapper--success.visible .cmp-footer__actions--completed .deny</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>.cmp-modal-share__wrapper--success.visible .cmp-footer__actions--completed .deny</value>
   </webElementProperties>
</WebElementEntity>
