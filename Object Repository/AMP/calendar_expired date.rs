<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>calendar_expired date</name>
   <tag></tag>
   <elementGuidId>291d16ed-c5a8-41f6-bb11-901fd348955c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'cartExpiryDate']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;cartExpiryDate&quot;]/div/input</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>cartExpiryDate</value>
   </webElementProperties>
</WebElementEntity>
