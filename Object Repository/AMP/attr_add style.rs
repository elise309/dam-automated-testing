<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>attr_add style</name>
   <tag></tag>
   <elementGuidId>f7e30bb5-abda-4839-8302-e5295c487d43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>ul li:nth-child(3) button:nth-child(1)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>ul li:nth-child(3) button:nth-child(1)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>style</name>
      <type>Main</type>
      <value>display: none;</value>
   </webElementProperties>
</WebElementEntity>
