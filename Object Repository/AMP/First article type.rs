<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>First article type</name>
   <tag></tag>
   <elementGuidId>0833056b-9d4e-4372-a6c4-c5f694ed647f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = '/content/amp/en/home/details/image.html/content/dam/product/9/2019_2/footwear/global/1011A636_750_SR_FL.png' and (text() = '
                    
                    
                        1011A636_750_SR_FL.png
                    
                ' or . = '
                    
                    
                        1011A636_750_SR_FL.png
                    
                ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//article[@id='/content/dam/projects/2019/2/camproject-3/footwear/1011A636_750/1011A636_750_SR_FL.png/1011A636_750_SR_FL.png']/div/h3/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>article:first .meta div:nth-child(2) span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//article[@id='/content/dam/projects/2019/2/camproject-3/footwear/1011A636_750/1011A636_750_SR_FL.png/1011A636_750_SR_FL.png']/div/h3/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'1011A636_750_SR_FL.png')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove From Cart'])[1]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to Cart'])[1]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/content/amp/en/home/details/image.html/content/dam/projects/2019/2/camproject-3/footwear/1011A636_750/1011A636_750_SR_FL.png/1011A636_750_SR_FL.png')])[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//article[2]/div/h3/a</value>
   </webElementXpaths>
</WebElementEntity>
