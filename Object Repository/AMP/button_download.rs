<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_download</name>
   <tag></tag>
   <elementGuidId>e9d0ad26-4872-4a5d-90ba-50344b006258</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul li:nth-child(1)</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//button[@class = 'ui link button' and @data-asset-share-id = 'download-asset' and @data-asset-share-asset = '/content/dam/product/9/2019_2/footwear/global/1011A636_750_SR_FL.png' and (text() = '
                        Download
                    ' or . = '
                        Download
                    ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//article[@id='/content/dam/product/9/2019_2/footwear/global/1011A636_750_SR_FL.png']/div[2]/ul/li/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//article[@id='/content/dam/product/9/2019_2/footwear/global/1011A636_750_SR_FL.png']/div[2]/ul/li/button</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='IMAGE'])[3]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Share'])[5]/preceding::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to Cart'])[6]/preceding::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//article[5]/div[2]/ul/li/button</value>
   </webElementXpaths>
</WebElementEntity>
