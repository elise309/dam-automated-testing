<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card_elements</name>
   <tag></tag>
   <elementGuidId>5e517608-b255-48e6-8825-7ef2449b7661</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'image cmp-image__wrapper--card']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>image cmp-image__wrapper--card</value>
   </webElementProperties>
</WebElementEntity>
