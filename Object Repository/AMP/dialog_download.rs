<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dialog_download</name>
   <tag></tag>
   <elementGuidId>a9727c39-17dd-472f-93ca-29575e0724c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'ui modal cmp-modal-download--wrapper cmp-modal transition visible active']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui modal cmp-modal-download--wrapper cmp-modal transition visible active</value>
   </webElementProperties>
</WebElementEntity>
