<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_add to cart</name>
   <tag></tag>
   <elementGuidId>8685f801-0457-46a8-abe6-1b2f6dce3fc4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//article[@id='/content/dam/product/9/2019_2/footwear/global/1011A636_750_SR_FL.png']/div[2]/ul/li[3]/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//button[@class = 'ui link button' and @data-asset-share-id = 'add-to-cart' and @data-asset-share-asset = '/content/dam/product/9/2019_2/footwear/global/1011A636_750_SR_FL.png' and (text() = '
                        Add to Cart
                    ' or . = '
                        Add to Cart
                    ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul li:nth-child(3) button:nth-child(1)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//article[@id='/content/dam/product/9/2019_2/footwear/global/1011A636_750_SR_FL.png']/div[2]/ul/li[3]/button</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Share'])[7]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download'])[7]/following::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove From Cart'])[7]/preceding::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//article[7]/div[2]/ul/li[3]/button</value>
   </webElementXpaths>
</WebElementEntity>
